# MINICURSO MOBILE #

Repositório de apresentação do minicurso + cronograma e conteúdos.

O objetivo deste minicurso é desmistificar um pouco mais sobre o desenvolvimento mobile e a persistência de dados utilizando arquitetura REST.
Para isso estaremos desenvolvendo um app mobile que consuma e persista dados de uma API (também desenvolvida por nós).
Se tudo ocorrer bem, também será desenvolvida uma versão web do app, o qual consumirá os dados de nossa API.


### CRONOGRAMA ###

#### 1º dia ####
- Apresentação de tecnologias e conceitos que serão utilizados no decorrer do minicurso
- Desenvolvimento da API, utilizando [Node](https://nodejs.org/en/), onde será criado um CRUD de Clifor que será persistido em uma instância do [MongoDB](https://www.mongodb.com/what-is-mongodb), localizado na nuvem através do serviço [MongoDB Atlas](https://www.mongodb.com/cloud)
- Início do desenvolvimento do app mobile (Android e iOS), utilizando [React Native](https://facebook.github.io/react-native/)

#### 2º dia ####
- Continuação do desenvolvimento mobile, onde será criada uma tela de login/cadastro + tela inicial + tela de listagem de Clifors (contendo modal de cadastro/alteração/exibição de Clifor)

#### 3º dia ####
- [se tudo ocorrer bem] Desenvolvimento de versão web do app, utilizando [Vue.js](https://vuejs.org/)
- Encerramento + discussões/debates entre os envolvidos


Feedbacks serão bem-vindos :)

### Links para os repositórios dos apps que serão criados ###
- [API](https://bitbucket.org/diego_m/mc-mobile-api)
- [App Mobile](https://bitbucket.org/diego_m/mc-mobile-app)
- [App Web](https://bitbucket.org/diego_m/mc-mobile-web)


### Links ###
- [Git](https://git-scm.com/downloads)
- [Node](https://nodejs.org)
- [NPM](https://www.npmjs.com)
- [Yarn](https://yarnpkg.com/pt-BR)
- [React Native](https://facebook.github.io/react-native)
- [Ignite](https://infinite.red/ignite)
- [MongoDB] (https://docs.mongodb.com/getting-started/shell/installation)


### Autor ###
[Diego Morgenstern](mailto:morgenstern.diego@gmail.com)